#!/usr/bin/env python2
__author__ = 'bnj'





#--------------------------------------
#    ___  ___  _ ____
#   / _ \/ _ \(_) __/__  __ __
#  / , _/ ___/ /\ \/ _ \/ // /
# /_/|_/_/  /_/___/ .__/\_, /
#                /_/   /___/
#
#  lcd_16x2.py
#  20x4 LCD Test Script with
#  backlight control and text justification
#
# Author : Matt Hawkins
# Date   : 06/04/2015
#
# http://www.raspberrypi-spy.co.uk/
#
#--------------------------------------

# The wiring for the LCD is as follows:
# 1 : GND
# 2 : 5V
# 3 : Contrast (0-5V)*
# 4 : RS (Register Select)
# 5 : R/W (Read Write)       - GROUND THIS PIN
# 6 : Enable or Strobe
# 7 : Data Bit 0             - NOT USED
# 8 : Data Bit 1             - NOT USED
# 9 : Data Bit 2             - NOT USED
# 10: Data Bit 3             - NOT USED
# 11: Data Bit 4
# 12: Data Bit 5
# 13: Data Bit 6
# 14: Data Bit 7
# 15: LCD Backlight +5V**
# 16: LCD Backlight GND

#import
import RPi.GPIO as GPIO
import time

# Define GPIO to LCD mapping
LCD_RS = 7
LCD_E  = 8
LCD_D0 = 26
LCD_D1 = 19
LCD_D2 = 13
LCD_D3 = 6
LCD_D4 = 25
LCD_D5 = 24
LCD_D6 = 23
LCD_D7 = 18
LED_ON = 15

# Define some device constants
LCD_WIDTH = 16    # Maximum characters per line
LCD_CHR = True
LCD_CMD = False

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line
LCD_LINE_3 = 0x94 # LCD RAM address for the 3rd line
LCD_LINE_4 = 0xD4 # LCD RAM address for the 4th line

# Timing constants
E_PULSE = 0.0005
E_DELAY = 0.0005


# Mode Constant
MODE_INCR = 0x02
MODE_DECR = 0x00

MODE_SHIFT = 0x01
MODE_NO_SHIFT = 0x00

# Display Constant
DISPLAY_ON = 0x04
DISPLAY_OFF = 0x00

CURSOR_ON = 0x02
CURSOR_OFF = 0x00

BLINK_ON = 0x01
BLINK_OFF = 0x00

# Shift Constant
SHIFT_DISPLAY = 0x08
SHIFT_CURSOR = 0x00

SHIFT_RIGHT = 0x04
SHIFT_LEFT = 0x00

# Set function constant
FUNC_4BITS_INT = 0x00
FUNC_8BITS_INT = 0x10

FUNC_1_LINE = 0x00
FUNC_2_LINE = 0x08

FUNC_5x10_DOTS = 0x04
FUNC_5x7_DOTS = 0x00


class LCD_DT161A:

    CMD_CLEAR = 0x01
    CMD_RETURN = 0x02
    CMD_MODE = 0x04
    CMD_DISPLAY = 0x08
    CMD_SHIFT = 0x10
    CMD_FUNCTION = 0x20
    CMD_CG_RAM = 0x40
    CMD_DD_RAM = 0x80

    def __init__(self):
        GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers
        GPIO.setup(LCD_E, GPIO.OUT)  # E
        GPIO.setup(LCD_RS, GPIO.OUT) # RS
        GPIO.setup(LCD_D0, GPIO.OUT) # DB4
        GPIO.setup(LCD_D1, GPIO.OUT) # DB5
        GPIO.setup(LCD_D2, GPIO.OUT) # DB6
        GPIO.setup(LCD_D3, GPIO.OUT) # DB7
        GPIO.setup(LCD_D4, GPIO.OUT) # DB4
        GPIO.setup(LCD_D5, GPIO.OUT) # DB5
        GPIO.setup(LCD_D6, GPIO.OUT) # DB6
        GPIO.setup(LCD_D7, GPIO.OUT) # DB7
        GPIO.setup(LED_ON, GPIO.OUT) # Backlight enable




    def write_byte(self, bits, mode):
        """ Send byte to data pins
        bits = data
        mode = True  for character
        False for command
        """

        GPIO.output(LCD_RS, mode) # RS

        # High bits
        GPIO.output(LCD_D0, False)
        GPIO.output(LCD_D1, False)
        GPIO.output(LCD_D2, False)
        GPIO.output(LCD_D3, False)
        GPIO.output(LCD_D4, False)
        GPIO.output(LCD_D5, False)
        GPIO.output(LCD_D6, False)
        GPIO.output(LCD_D7, False)
        if bits&0x10==0x10:
            GPIO.output(LCD_D4, True)
        if bits&0x20==0x20:
            GPIO.output(LCD_D5, True)
        if bits&0x40==0x40:
            GPIO.output(LCD_D6, True)
        if bits&0x80==0x80:
            GPIO.output(LCD_D7, True)

        # Low bits
        if bits&0x01==0x01:
            GPIO.output(LCD_D0, True)
        if bits&0x02==0x02:
            GPIO.output(LCD_D1, True)
        if bits&0x04==0x04:
            GPIO.output(LCD_D2, True)
        if bits&0x08==0x08:
            GPIO.output(LCD_D3, True)

        # Toggle 'Enable' pin
        self.lcd_toggle_enable()



    def write_cmd(self, cmd):
        self.write_byte(cmd, LCD_CMD)

    def lcd_toggle_enable(self):
        """ Toggle enable """

        time.sleep(E_DELAY)
        GPIO.output(LCD_E, True)
        time.sleep(E_PULSE)
        GPIO.output(LCD_E, False)
        time.sleep(E_DELAY)

    # Basic LCD Command

    def clear(self):
        self.write_cmd(self.CMD_CLEAR)

    def return_home(self):
        self.write_cmd(self.CMD_RETURN)

    def set_entry_mode(self, move_direction, shifting):
        self.write_cmd(self.CMD_MODE + move_direction + shifting)

    def display_mode(self,display, cursor, blinking):
        self.write_cmd(self.CMD_DISPLAY + display + cursor + blinking)

    def shift(self, display_cursor, way):
        self.write_cmd(self.CMD_SHIFT + display_cursor + way)

    def set_function(self, nb_bits, nb_line, font_size):
        self.write_cmd(self.CMD_FUNCTION + nb_bits + nb_line + font_size)

    def set_CGRAM_address(self, address):
        self.write_cmd(self.CMD_CG_RAM + address)

    def set_DDRAM_address(self, address):
        self.write_cmd(self.CMD_DD_RAM + address)

    def get_busy_address(self):
        return (1,1)


    def write(self, data):
        pass


    def read(self):
        return 0

    # High Level Functions

    def write_string(self, message, line, style):
        """ Send string to display
            style=1 Left justified
            style=2 Centred
            style=3 Right justified """

        if style==1:
            message = message.ljust(LCD_WIDTH," ")
        elif style==2:
            message = message.center(LCD_WIDTH," ")
        elif style==3:
            message = message.rjust(LCD_WIDTH," ")

        self.write_byte(0x00, LCD_CMD)

        for i in range(LCD_WIDTH/2):
            self.write_byte(ord(message[i]),LCD_CHR)

        self.write_byte(0xC0, LCD_CMD)

        for i in range(LCD_WIDTH/2,LCD_WIDTH):
            self.write_byte(ord(message[i]),LCD_CHR)



def lcd_init(lcd):
    # Initialise display
    lcd.write_byte(0x30,LCD_CMD) # 110011 Initialise
    lcd.write_byte(0x30,LCD_CMD) # 110011 Initialise
    lcd.write_byte(0x30,LCD_CMD) # 110010 Initialise
    lcd.set_function(FUNC_8BITS_INT, FUNC_2_LINE, FUNC_5x7_DOTS)   # 101000 Data length, number of lines, font size
    lcd.display_mode(DISPLAY_OFF,CURSOR_OFF,BLINK_OFF)   # Display On,Cursor On, Blink On
    lcd.clear()
    lcd.set_entry_mode(MODE_INCR,MODE_NO_SHIFT)     # Cursor move direction
    lcd.display_mode(DISPLAY_ON,CURSOR_ON,BLINK_ON)   # Display On,Cursor On, Blink On

    lcd.clear()   # Clear display

    time.sleep(E_DELAY)

if __name__ == '__main__':

    lcd = LCD_DT161A()
    # Initialise display
    lcd_init(lcd)


    time.sleep(10)   # 3 second delay



    # Send some centred test
    lcd.write_string("----------------",LCD_LINE_1,2)
    time.sleep(3)   # 3 second delay
    lcd.write_string("Rasbperry Pi",LCD_LINE_1,2)
    time.sleep(3)   # 3 second delay
    lcd.write_string("Model B",LCD_LINE_1,2)
    time.sleep(3)   # 3 second delay
    lcd.write_string("----------------",LCD_LINE_1,2)
    time.sleep(3)   # 3 second delay
    lcd.write_string("EWFT",LCD_LINE_1,3)
    time.sleep(3)   # 3 second delay
    lcd.write_string(".org",LCD_LINE_1,3)
    time.sleep(3)   # 3 second delay
    lcd.write_string("",LCD_LINE_1,2)
    time.sleep(3)   # 3 second delay
    lcd.write_string("20x4 LCD Module ",LCD_LINE_1,2)

    time.sleep(3)   # 20 second delay

    # Blank display
    lcd.clear()
    time.sleep(3)   # 3 second delay
    lcd.clear()
    lcd.write_string("Goodbye!",LCD_LINE_1,2)
    GPIO.cleanup()